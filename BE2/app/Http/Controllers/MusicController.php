<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MusicController extends Controller
{
    public function upload(){
        return view('page.upload');
    }
    public function create(){
        return view('page.create');
    }
    public function addlist(){
        return view('page.addlist');
    }
}
