<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PagesController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/*
Route::get('/world', function () {
      return 'Hello World';
    });
 */

Route::get('/upload', 'MusicController@upload');
Route::get('/create', 'MusicController@create');   
Route::get('/addlist', 'MusicController@addlist');

Route::get('/upload', function(){
    return view('page.upload');
    });
    Route::get('/create', function(){
        return view('page.create');
        });
    Route::get('/addlist', function(){
        return view('page.addlist');
            });
    